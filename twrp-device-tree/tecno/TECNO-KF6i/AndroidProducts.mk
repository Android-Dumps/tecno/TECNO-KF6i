#
# Copyright (C) 2021 The Android Open Source Project
# Copyright (C) 2021 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_TECNO-KF6i.mk

COMMON_LUNCH_CHOICES := \
    omni_TECNO-KF6i-user \
    omni_TECNO-KF6i-userdebug \
    omni_TECNO-KF6i-eng
